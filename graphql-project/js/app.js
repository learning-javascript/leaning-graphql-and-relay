import React from 'react';
import ReactDOM from 'react-dom';

import Quote from './quote';

class QuoteLibrary extends React.Component {
  state = { allQuotes: [] };

  componentDidMount() {
    // Load the quotes list into this.state.allQuotes
    fetch(`/graphql?query={
      allQuotes {
        id,
        text,
        author
      }
    }`)
      .then(response => response.json())
      .then(json => this.setState(json.data))
      .catch(err => console.log(err));
  }

  render() {
    return (
      <div className="quotes-list">
        {this.state.allQuotes.map(quote =>
          <Quote key={quote.id} quote={quote}/>
        )}
      </div>
    );
  }
}

ReactDOM.render(
  <QuoteLibrary/>,
  document.getElementById('react')
);
