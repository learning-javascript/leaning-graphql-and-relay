const {GraphQLSchema, GraphQLObjectType, GraphQLString, GraphQLList, GraphQLInt} = require('graphql');

const QuoteType = new GraphQLObjectType({
  name: 'Quote',
  fields: {
    id: {
      type: GraphQLString,
      resolve: obj => obj._id
    },
    text: {
      type: GraphQLString
    },
    author: {
      type: GraphQLString
    }
  }
});

const queryType = new GraphQLObjectType({
  name: 'RootQuery',
  fields: {
    allQuotes: {
      description: 'A list of the quotes in the database',
      type: new GraphQLList(QuoteType),
      resolve: (_, args, {db}) => db.collection('quotes').find().toArray()
    }
  }
});

const mySchema = new GraphQLSchema({
  query: queryType
});

module.exports = mySchema;
