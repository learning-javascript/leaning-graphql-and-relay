const {MongoClient} = require('mongodb');
const assert = require('assert');
const graphqlHTTP = require('express-graphql');
const express = require('express');

const app = express();
app.use(express.static('public'));
const mySchema = require('./schema/main');
const MONGO_URL = 'mongodb://localhost:27017';
const dbName = 'test';

MongoClient.connect(MONGO_URL, (err, client) => {
  assert.equal(null, err);
  console.log('Connected to MongoDB server');
  const db = client.db(dbName);

  app.use('/graphql', graphqlHTTP({
    schema: mySchema,
    context: {db},
    graphiql: true
  }));

  app.listen(3000, () => {
    console.log('Running express on port 3000');
  });
});
